package nl.utwente.di.bookQuote;

public class Quoter {
    public String getTemperature(String isbn){
        double temp = Double.parseDouble(isbn);
        temp = temp*1.8+32;
        return String.valueOf(temp);
    }
}
